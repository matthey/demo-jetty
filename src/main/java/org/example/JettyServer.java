package org.example;

import static org.example.Constants.INFO_MSG_TEMPLATE;

import java.io.IOException;
import java.io.InputStreamReader;

import org.eclipse.jetty.server.Request;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.handler.AbstractHandler;
import org.eclipse.jetty.server.handler.HandlerList;

import com.google.gson.Gson;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

/**
 *
 */
public class JettyServer
{
    /**
     * Обработчик
     */
    static class JettyHandler extends AbstractHandler
    {
        private final String path;
        private final String method;

        public JettyHandler(String path, String method)
        {
            this.path = path;
            this.method = method;
        }

        @Override
        public void handle(
                String target,
                Request baseRequest,
                HttpServletRequest request,
                HttpServletResponse response
        ) throws IOException
        {
            if (!target.equals(path) || !baseRequest.getMethod().equals(method))
                return;

            // считываем
            final User user;
            try (var inputStream = baseRequest.getInputStream();
                 var reader = new InputStreamReader(inputStream))
            {
                user = GSON.fromJson(reader, User.class);
            }

            final var msg = INFO_MSG_TEMPLATE.formatted(user.toString());
            System.out.println(msg);

            // отправляем ответ
            try (final var writer = response.getWriter())
            {
                writer.println(msg);
            }
            response.setContentType("text/html;charset=utf-8");
            response.setStatus(HttpServletResponse.SC_OK);
            baseRequest.setHandled(true);
        }
    }

    private static final Gson GSON = new Gson();
    private final Server jettyServer;

    public JettyServer(int port) throws Exception
    {
        jettyServer = new Server(port);
        final var handlerList = new HandlerList(
                new JettyHandler("/upload_user", "POST")
        );
        jettyServer.setHandler(handlerList);
        jettyServer.setStopAtShutdown(true);
        jettyServer.start();
    }

    public void stop() throws Exception
    {
        jettyServer.stop();
    }
}