package org.example;

import static org.example.Constants.INFO_MSG_TEMPLATE;
import static org.example.Constants.SUP_MSG;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.nio.charset.StandardCharsets;

import com.google.gson.Gson;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;

import jakarta.servlet.http.HttpServletResponse;

public class SunServer
{
    static class SunHttpHandler implements HttpHandler
    {
        private final String method;
        public SunHttpHandler(String method)
        {
            this.method = method;
        }

        @Override
        public void handle(HttpExchange exchange) throws IOException
        {
            // обрабатываю только POST методы
            if (!exchange.getRequestMethod().equals(method))
            {
                try (OutputStream os = exchange.getResponseBody())
                {
                    exchange.sendResponseHeaders(HttpServletResponse.SC_METHOD_NOT_ALLOWED, SUP_MSG.length());
                    os.write(SUP_MSG.getBytes(StandardCharsets.UTF_8));
                    return;
                }
            }

            // считываем
            final User user;
            try (final var requestBody = exchange.getRequestBody();
                 final var inputStreamReader = new InputStreamReader(requestBody))
            {
                user = GSON.fromJson(inputStreamReader, User.class);
            }
            final var msg = INFO_MSG_TEMPLATE.formatted(user.toString());
            System.out.println(msg);

            // закрываем стрим ответа
            try (OutputStream os = exchange.getResponseBody())
            {
                exchange.sendResponseHeaders(HttpServletResponse.SC_OK, msg.length());
                os.write(msg.getBytes(StandardCharsets.UTF_8));
            }
        }
    }

    private static final Gson GSON = new Gson();
    private final HttpServer httpServer;
    public SunServer(int port) throws IOException
    {
        httpServer = HttpServer.create(new InetSocketAddress(port), 0);
        httpServer.createContext("/upload_user", new SunHttpHandler("POST"));
        httpServer.start();
    }


    public void close()
    {
        httpServer.stop(1);
    }

    public int getPort()
    {
        return httpServer.getAddress().getPort();
    }
}